<?php

/**
 * @copyright  Jens Doberenz 2018 <https://jedo-style.de>
 * @author     Jens Doberenz
 * @package    jedostyle/fontawesome-icon-picker-bundle
 * @license    LGPL-3.0+
 * @see	       https://github.com/jedoStyle/company-core-bundle
 *
 */


use Contao\CoreBundle\DataContainer\PaletteManipulator;


PaletteManipulator::create()
    ->addLegend('FIP_FontAwesomeSRC_legend:hide', 'chmod_legend:hide', PaletteManipulator::POSITION_AFTER)
    ->addField('FIP_FontAwesomeSRC', 'FIP_FontAwesomeSRC_legend:hide', PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_settings')
;

// Fields
$GLOBALS['TL_DCA']['tl_settings']['fields']['FIP_FontAwesomeSRC'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_settings']['FIP_FontAwesomeSRC'],
    'exclude'       => true,
    'inputType'     => 'fileTree',
    'eval'          => array('fieldType'=>'radio', 'tl_class'=>'w50 clr','mandatory'=>true, 'helpwizard'=>true ),
    'explanation'   => 'FontAwesomeSRC'
);

